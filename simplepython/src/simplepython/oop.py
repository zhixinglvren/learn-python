# 面向对象编程
# 类与对象
# a) 类是一种类型
# b) 对象是类的实例
# c) 对象可以使用属于它的普通变量来存储数据。这种从属于对象或类的变量叫作字段（Field）
# d) 对象还可以使用属于类的函数来实现某些功能，这种函数叫作类的方法（Method）
# e) 字段与方法通称类的属性（Attribute）
# f) 字段有两种类型——它们属于某一类的各个实例或对象，或是从属于某一类本身。它们被分别称作实例变量（Instance Variables）与类变量（Class Variables）
# g) 类方法与普通函数只有一种特定的区别——前者必须多加一个参数在参数列表开头，这个名字必须添加到参数列表的开头，但是你不用在你调用这个功能时为这个参数赋值，Python 会为它提供。这种特定的变量引用的是对象本身，按照惯例，它被赋予 self 这一名称。


class Person:
    pass
p = Person()
print(p)


class Person:
    def say_hi(self):
        print('Hello,how are you?')
p = Person()
p.say_hi()

Person().say_hi()

class Person:
    def __init__(self,name):
        self.name = name
        print('Hello,how are you?')

    def say_hi(self):
        print('Hello,my name is',self.name)
p = Person('Sam')
p.say_hi()

# 类变量（Class Variable）是共享的（Shared）——它们可以被属于该类的所有实例访问。
# 对象变量（Object variable）由类的每一个独立的对象或实例所拥有。


