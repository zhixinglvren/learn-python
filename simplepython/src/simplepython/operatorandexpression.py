#运算符与表达式


# + (加)
print(3 + 4) # result = 7
print('a' + 'b') # result = ab

# - (减)
print(5 - 3) # result = 2
#print('a' - 'b') # result：TypeError: unsupported operand type(s) for -: 'str' and 'str'
#print('a' - 3) # result: TypeError: unsupported operand type(s) for -: 'str' and 'int'

# * (乘)
print(6 * 3) # result = 18
#print('a' * 'b') #result: TypeError: can't multiply sequence by non-int of type 'str'

# ** (乘方) 返回 x 的 y 次方
print(2 ** 3) # result = 8

# / (除法)
print(12 / 4) # result = 3.0

# // (整除) x 除以 y 并对结果向下取整至最接近的整数
print(13 // 4) # result = 3

# % (取模) 返回除法运算后的余数
print(13 % 3) # result = 1

# << (左移)
print(3 << 2) #result = 12 3的二进制为011，左移两位2为01100

# >> (右移)
print(2 >> 3) #result = 0

# & (按位与)
print(3 & 5) #result = 1 3的二进制为011，5的二进制位101，同进制位1 & 1 = 1，1 & 0 = 0，0 & 0 = 0

# | (按位或)
print(3 | 5) #result = 7 3的二进制为011，5的二进制位101，同进制位1 | 1 = 1，1 | 0 = 1，0 | 0 = 0

# ^ (按位异或)
print(3 ^ 5) #result = 6 3的二进制为011，5的二进制位101，同进制位1 ^ 1 = 0，1 ^ 0 = 1，0 ^ 0 = 0

# ~ (按位取反)
print(~ 5) #result = -6 按位取反结果为 -(x+1) ???

# < (小于)
print(3 < 5) #result = True

# > (大于)
print(3 > 5) #result = False

# <= (小于等于)
print(3 <= 5) #result = True

# >= (大于等于)
print(3 >= 5) #result = False

# == (等于等于)
print(3 == 5) #result = False

# != (不等于)
print(3 != 5) #result = True

# not (布尔'非')
print(not(3 < 5)) #result = False

# and (布尔'与')
print(3 < 5 and 2 < 4) #result = True

# and (布尔'或')
print(3 < 5 or 3 > 5) #result = True

a = 2
a = 2 * a
a *= 2

#表达式优先级