# 控制流


number = 20

# if elif else
guess = int(input('Please Guess The Number:'))
print('You Guess Number Is {}'.format(guess))
if guess < number:
    print('Answer:Wrong,You Guess A Little Lower!')
elif guess > number:
    print('Answer:Wrong,You Guess A Little Higher!')
else:
    print('Answer:Right,Congratulations!')

# while
guessFlag = True
while guessFlag:
    guess = int(input('Please Guess The Number:'))
    if guess < number:
        print('Answer:Wrong,You Guess A Little Lower!')
    elif guess > number:
        print('Answer:Wrong,You Guess A Little Higher!')
    else:
        guessFlag = False
        print('Answer:Right,Congratulations!')
else:
    print('While Loop Over')

# for
for i in range(1,5):  #从1开始，不包括5
    print('i={}'.format(i))

for j in list(range(1,5)):
    print('j={}'.format(j))
else:
    print('For Loop Over')

# break
while guessFlag:
    guess = int(input('Please Guess The Number:'))
    if guess == number:
        print('Break While')
        break
    print('While No Break')
print('Done')

# continue
for j in range(1,10):
    if j == 3:
        continue
    else:
        print('j={}'.format(j))
else:
    print('Done')