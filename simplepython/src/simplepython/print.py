# 基础


print("{0} is out best {1},and he is so nice!".format("Tony","teacher"))

print("{} is out best {},and he is so nice!".format("Tony","teacher"))

#关键词输出
print("{name} is out best {teacher},and he is so nice!".format(name="Tony",teacher="teacher"))

#浮点数保留5位有效数字
print("{0:.5f}".format(1.0/3))

#使用下划线填充文本，并使文本处于中间位置
print("{0:_^10}".format("hello"))

print("a",end="")
print("b",end="")

print("\n")

print("a",end=" ")
print("b",end=" ")

print("a",end=";")
print("b",end=";")

#多行文本
print(
'''
Tim:Hi,what's your name?
Andy:Hi,my name is Andy.
Tim:Nice to meet you.
Andy:me too!
'''
)

#一个放置在末尾的反斜杠表示字符串将在下一行继续，但不会添加新的一行
print(
"Tim:Hi,what's your name?\
Andy:Hi,my name is Andy.\
Tim:Nice to meet you.\
Andy:me too!"
)

#转义序列
print('what\'s your name\n')
print('where are\\you?\n')
print("Tim:Hi,what's your name?\nAndy:Hi,my name is Andy.\nTim:Nice to meet you.\nAndy:me too!\n")

#原始字符串
print(r"Who want to go with me \n")
print(R"Who want to go with me \n")

i=5
    #print('Value is ',i)  # IndentationError: unexpected indent
print('Value is ',i)

