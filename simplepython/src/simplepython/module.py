# 模块


import sys

print('The command line arguments are:')
for i in sys.argv:
    print(i)

print('\n\nThe Python-Path is',sys.path,'\n')

# python D:/IDEAUWorkspace/codestudy/learn-python/src/simplepython/module.py we are okay

from math import sqrt
print('Square 2 Is',sqrt(16))



if __name__ == '__main__':
    print('This program is being run by itself')
else:
    print('I am being imported from another module')

#dir函数
#内置的 dir() 函数能够返回由对象所定义的名称列表。 如果这一对象是一个模块，则该列表会包括函数内所定义的函数、类与变量
print(dir())
print(dir(sys))
newargs='newargs'
print(dir())
del newargs
print(dir())


#包
#包是用以组织模块的另一种层次结构