import os
import time
import zipfile

# 需要备份的文件与目录将被指定在一个列表中。
source = ['D:\\nfs\\wanjia\\settings\\igw']

# 备份文件必须存储在一个主备份目录中
target_dir = 'D:\\nfs\\wanjia\\backup'
# 备份文件将打包压缩成 zip 文件
# 将当前日期作为主备份目录下的子目录名称
today = target_dir + os.sep + time.strftime('%Y%m%d')
# 将当前时间作为 zip 文件的文件名
now = time.strftime('%H%M%S')
# zip 文件名称格式
comment = input('Please Enter a comment -->')
if len(comment) == 0:
    target = today + os.sep + now + '.zip'
else:
    target = today + os.sep + now + '_' + comment.replace(' ','_') + '.zip'
# 如果目标目录还不存在，则进行创建
if not os.path.exists(today):
    os.mkdir(today)
    print('Successfully created directory',today)
# 我们使用 zipfile 命令将文件打包成 zip 格式
z = zipfile.ZipFile(target,'w',zipfile.ZIP_DEFLATED)
# 运行备份
print('Running:')
for sdir in source:
    if os.path.isdir(sdir):
        for file in os.listdir(sdir):
            print('zip file:' + sdir + os.sep + file)
            z.write(sdir + os.sep + file)
            z.close()
            print('Successful backup to',target)
    else:
        print('Backup FAILED:' + sdir + 'is not dir')