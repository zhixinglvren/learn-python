# 函数


def print_mylogo():
    print('My Logo Is MyLogo')

print_mylogo();

print_mylogo();


#函数参数
def print_max(a,b):
    if a > b:
        print('a is the maximum')
    elif a == b :
        print('a equals b')
    else :
        print('b is the maximum')

print_max(4,5)

#局部变量
str = 'abc'

def print_str(str) :
    print('str is',str) #abc
    str = 'ABC'
    print('str Change to',str) #ABC

print_str(str)
print('now str is',str) #abc


#全局变量
tim = 'abc'

def print_tim() :
    global tim
    print('tim is',tim) #abc
    tim = '123abc'
    print('tim Change to',tim) #123abc

print_tim()
print('now tim is',tim) #123abc


#默认参数
def defaultargs(word,times=1):
    print(word * times)

defaultargs('World') #World
defaultargs('World',5) #WorldWorldWorldWorldWorld

#关键字参数
def keyargs(a,b=5,c=6):
    print('a is',a,'and b is',b,'and c is',c)

keyargs(a=10,b=6,c=7) #a is 10 and b is 6 and c is 7
keyargs(10,5) #a is 10 and b is 5 and c is 6
keyargs(c=20,a=8) #a is 8 and b is 5 and c is 20

#可变参数
def varargs(a,*numbers,**contacts):
    print(a)

    #遍历元组中所有的项目
    for single_item in numbers:
        print('single_item',single_item)

    #遍历字典中所有的项目
    for first_part,second_part in contacts.items():
        print('first_part',first_part,'second_part',second_part)

print(varargs(20,1,2,3,Jack=120,John=123,Tony=1241)) # None
# None 在 Python 中一个特殊的类型，代表着虚无,每一个函数都在其末尾隐含了一句 return None


#return
def returnfunc(a,b):
    '''
    :param a:参数1
    :param b: 参数2
    :return: 返回a和b中较大的那个数，当两个值相等时，返回a和b
    '''
    if a > b:
        return a
    elif a == b:
        return a,b
    else:
        return b

result = returnfunc(5,7)
print('Maxnum Is',result) #Maxnum Is 7

result = returnfunc(6,6)
print('Maxnum Is',result) #Maxnum Is (6, 6)


def passfunc():
    pass  #Python 中的 pass 语句用于指示一个没有内容的语句块
passfunc()


#DocStrings
print(returnfunc.__doc__)